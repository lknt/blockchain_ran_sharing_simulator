%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [signalApAp, signalApSta, signalStaSta] = ComputeSignalReceived(deployment)
%ComputeSignalReceived computes the signal received by STAs and APs
% INPUT:
%   * deployment: Deployment object
% OUTPUT:
%   * signalApAp: signal received by APs from other APs
%   * signalApSta: signal received by STAs from other APs
%   * signalStaSta: signal received by STAs from other STAs

    PLd1=5;           % Path-loss factor
    shadowing = 9.5;  % Shadowing factor
    obstacles = 30;   % Obstacles factor
    alfa = 4.4;
        
    signalApAp = zeros(deployment.nAps,deployment.nAps);
    signalApSta = zeros(deployment.nAps,deployment.nStas);
    signalStaSta = zeros(deployment.nStas,deployment.nStas);
    
    for i = 1 : deployment.nAps

        for j = 1 : deployment.nAps
            signalApAp(i,j) = deployment.TxPower - ...
                (PLd1 + 10 * alfa * log10(deployment.distApAp(i,j)) + ...
                shadowing / 2 + (deployment.distApAp(i,j)/10) .* obstacles / 2);
        end

        for n = 1 : deployment.nStas
            signalApSta(i,n) = deployment.TxPower - ...
                (PLd1 + 10 * alfa * log10(deployment.distApSta(i,n)) + ...
                shadowing / 2 + (deployment.distApSta(i,n)/10) .* obstacles / 2);
        end   

    end
    
    for i = 1 : deployment.nStas
        for n = 1 : deployment.nStas
            signalStaSta(i,n) = deployment.TxPower - ...
                (PLd1 + 10 * alfa * log10(deployment.distStaSta(i,n)) + ...
                shadowing / 2 + (deployment.distStaSta(i,n)/10) .* obstacles / 2);
        end   
    end

end