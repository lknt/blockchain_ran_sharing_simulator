%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function users = GenerateUsers(deployment)
%GenerateUsers generates a users in a wireless deployment
%   INPUT
%   - deployment: object containing the information of the deployment
%   - R: radius of each cell
%   OUTPUT
%   - users: users object

    load('conf_simulation.mat')
    
    service_duration = minServiceDuration:deltaServiceDuration:maxServiceDuration;
    throughput_requirement = minThroughputReq:deltaThroughputReq:maxThroughputReq;
    delay_requirement = minDelayReq:deltaDelayReq:maxDelayReq;

    service_sensitivity = 0.01:0.01:.2;
    price_sensitivity = 0.01:0.01:0.2;

    for i = 1 : deployment.nStas
        % User ID
        users(i).user_id = i;
        % Service duration
        users(i).averageServiceDuration = service_duration(randi(length(service_duration)));
        % Throughput requirements
        users(i).averageThroughputRequirement = throughput_requirement(randi(length(throughput_requirement)));
        % Delay requirements
        users(i).averageDelayRequirement = delay_requirement(randi(length(delay_requirement)));
        % Set the initial max. price for resource acquisition
        users(i).max_price_per_unit = rand()*maxPricePerServiceUnit;   
        % Throughput requirements
        users(i).serviceSensitivity = service_sensitivity(randi(length(service_sensitivity)));
        % Throughput requirements
        users(i).priceSensitivity = price_sensitivity(randi(length(price_sensitivity)));
    end
    
end

