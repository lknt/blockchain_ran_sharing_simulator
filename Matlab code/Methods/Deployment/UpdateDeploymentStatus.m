%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [deployment] = UpdateDeploymentStatus(deployment, ...
    user_id, active_flag, channel_selected, ap_selected)
%UpdateDeploymentStatus update the status of the deployment based on
% changes in users' activity and channel allocations
% INPUT:
%   * deployment: object containing information about the deployment
%   * user_id: identifier of the user of interest
%   * active_flag: indicates whether the user is active or not
%   * channel_selected: channel selected by the user
%   * ap_selected: AP selected by the user
% OUTPUT:
%   * deployment: updated deployment object

    deployment.activeStas(user_id) = active_flag;
    deployment.channelStas(user_id) = channel_selected;
    deployment.apStas(user_id) = ap_selected;
        
end