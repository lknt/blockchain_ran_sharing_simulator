%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [] = WriteDeploymentDetails(LOGS_ENABLED, logs_file, deployment)
%WriteDeploymentDetails writes deployment details into a logs file
% INPUT:
%   * LOGS_ENABLED: flag indicating whether logs are enabled or not
%   * logs_file: file object to be written
%   * deployment: Deployment object

    load('constants.mat')
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL1 'Deployment details:']);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL2 'Number of APs/BSs: ' num2str(deployment.nAps)]);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Number of STAs: ' num2str(deployment.nStas)]);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Max. distance between APs: ' num2str(max(max(deployment.distApAp))) ' m']);
    max_d = 0;
    min_rx = 0;
    for i = 1 : deployment.nStas
        if deployment.distApSta(deployment.staNearestAp(i),i) > max_d
            max_d = deployment.distApSta(deployment.staNearestAp(i),i);
        end 
        if deployment.signalApSta(deployment.staNearestAp(i),i) < min_rx
            min_rx = deployment.signalApSta(deployment.staNearestAp(i),i);
        end 
    end
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Max. distance between STAs and the nearest AP: ' num2str(max_d) ' m']);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Min. rx signal at STAs from the nearest AP: ' num2str(min_rx) ' dBm']);

end

