%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [ap_id, dist_from_ap] = FindBestAp(deployment, user_id, operator)
%FindBestAp finds the best AP for a given user
% INPUT:
%   * deployment: object containing information about the deployment
%   * user_id: identifier of the user of interest
%   * operator: operator to deliver service to the user
% OUTPUT:
%   * ap_id: identifier of the best AP
%   * dist_from_ap: distance between the user and the best AP in meters

    % Find closest AP from operator
    dist = zeros(1,length(operator.ownedAps));
    for a = 1 : length(operator.ownedAps)    
        dist(a) = deployment.distApSta(operator.ownedAps(a), user_id);
    end
    
    [d, ix] = min(dist);    
    
    dist_from_ap = d;
    ap_id = operator.ownedAps(ix);
    
    if isempty(ap_id)
        ap_id = -1;
    end
    
end