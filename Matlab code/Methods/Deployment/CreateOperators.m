%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [deployment, operators] = CreateOperators(deployment, nOperators, ownedApsRatio)
% CreateOperators creates operators
% INPUT:
%   * deployment: object containing information about the deployment
%   * nOperators: number of operators in the deployment
%   * ownedApsRatio: ratio of the APs owned by each operator
% OUTPUT:
%   * deployment: updated deployment oject
%   * operators: operators object

    load('conf_simulation.mat')

    % Define APs' ownership
    aps_indices_random = randperm(deployment.nAps); % Randomize order of APs
    n = 1;
        
    for i = 1 : nOperators
        operators(i).required_spectrum = zeros(1, deployment.nAps);
        for j = 1 : deployment.nAps
            operators(i).subscribed_users{j} = [];
            operators(i).users_requirements{j} = [];
            operators(i).users_service_time{j} = [];
        end        
        operators(i).operator_id = i;
        operators(i).spectrumAps = zeros(1, deployment.nAps);
        numAps = deployment.nAps*ownedApsRatio(i); %deployment.nAps/nOperators;
        if i < nOperators
            operators(i).ownedAps = aps_indices_random(n:n+numAps-1);
        else
            if n < deployment.nAps
                operators(i).ownedAps = aps_indices_random(n:end);                
            end
        end
        operators(i).spectrumAps(operators(i).ownedAps) = 1;
        n = n+numAps-1;
    end
        
    % Allocate frequency resources
    %    - WiFi: different operators CAN use the same freq. resources
    %    - Cellular: different operators CANNOT use the same freq. resources
    switch PLANNING_MODE
        
        %   - Channels are allocated randomly
        case 1  %WIFI_SINGLE_CHANNEL_RANDOM
            
            for i = 1 : nOperators
                operators(i).channelRange = 1 : NUM_CHANNELS_SYSTEM;
                operators(i).channelPerAp = zeros(1,deployment.nAps);
                for j = 1 : length(operators(i).ownedAps)
                    selected_channel = operators(i).channelRange(randi(length(operators(i).channelRange)));
                    operators(i).channelPerAp(operators(i).ownedAps(j)) = selected_channel;
                end
            end
        %   - The same channel is allocated to all the APs               
        case 2 %WIFI_SINGLE_CHANNEL_ALL_SAME
            
            for i = 1 : nOperators
                operators(i).channelRange = 1;
                selected_channel = 1;
                for j = 1 : length(operators(i).ownedAps)
                    operators(i).channelPerAp(operators(i).ownedAps(j)) = selected_channel;
                end
            end
        %   - Unknown case (display error message)
        otherwise
            disp('Error: An unknown resource planning mode was introduced');
           
    end
    
    % Update deployment with new information
    for i = 1 : deployment.nAps
        for o = 1 : length(operators)
            if sum(find(operators(o).ownedAps==i))
                deployment.operatorAps(i) = o;
                deployment.channelAps(i) = operators(o).channelPerAp(i);
            end
        end
    end
    
    % AUCTION
    for i = 1 : length(operators)
        % Set the initial min. price for resource selling
        operators(i).min_price_per_unit = rand()*minPricePerServiceUnit;
        % Set the initial seller's attitude (1:neutral, >1:risky, <1:conservative)
        operators(i).attitude = sellerAttitude;        
        % Set the initial min. price for resource selling
        operators(i).min_price_per_spectrum = 1;
        % Set the initial seller's attitude (1:neutral, >1:risky, <1:conservative)
        operators(i).attitude = 1;        
        % Set the initial max. price for resource acquisition
        operators(i).max_price_per_spectrum = 1;   
    end
      
end