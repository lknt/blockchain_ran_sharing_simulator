%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [throughput_stas, spectrum_stas, load_ap, required_load_ap] = ComputeCapacity(deployment, operators)
%ComputeThroughput computes the throughput obtained by all the STAs and APs
% in a wireless deployment
% INPUT:
%   * deployment: Deployment object
%   * link_type: integer indicating the type of link used in wireless comm.
%   * interference_mode: integer indicating the way interference is considered
% OUTPUT:
%   * throughput_stas: throughput obtained by STAs
%   * throughput_ap: throughput obtained by APs

    load('constants.mat')
    
    % Initialize variables
    throughput_stas = zeros(1,deployment.nStas);
    spectrum_stas = zeros(1,deployment.nStas);
    load_ap = zeros(1,deployment.nAps);
    required_load_ap = zeros(1,deployment.nAps);
    
    % Check throughput of users from each operator
    for o = 1 : length(operators)
%         disp(['OPERATOR ' num2str(o)])
        % Iterate for each AP
        for a = 1 : deployment.nAps
            users_per_ap = operators(o).subscribed_users{a};
            % Allocate the maximum possible spectrum required by each user
            allocated_bw_stas = zeros(1, length(users_per_ap));
%             disp([' - Spectrum in AP ' num2str(a) ': ' num2str(operators(o).spectrumAps(a))])   
            required_load_ap(a) = required_load_ap(a) + sum(operators(o).users_requirements{a});
            % Iterate for each associated user
            if ~isempty(users_per_ap) && operators(o).spectrumAps(a) > 0
                spectrum_required_per_ap = sum(operators(o).users_requirements{a});
                % Allocate the maximum possible spectrum required by each user
                allocated_bw_stas = zeros(1, length(users_per_ap));
                if spectrum_required_per_ap < operators(o).spectrumAps(a)
                    for u = 1 : length(users_per_ap)
                        if isempty(operators(o).users_requirements{a})
                            allocated_bw_stas(u) = 0;
                        else
                            allocated_bw_stas(u) = operators(o).users_requirements{a}(u);
                        end
                    end
                else
                    for u = 1 : length(users_per_ap) 
                        if isempty(operators(o).users_requirements{a})
                            allocated_bw_stas(u) = 0;
                        else
                            allocated_bw_stas(u) = operators(o).spectrumAps(a) * ...
                                operators(o).users_requirements{a}(u) / ...
                                spectrum_required_per_ap;
                        end
                    end
                end                
%                 disp(['Users in AP ' num2str(a) ': ' num2str(users_per_ap)]); 
%                 disp([' - spectrum required ' num2str(spectrum_required_per_ap)]); 
%                 disp([' - spectrum available ' num2str(operators(o).spectrumAps(a))]); 
%                 disp([' - allocated BW ' num2str(allocated_bw_stas)]);

                load_ap(a) = load_ap(a) + min(operators(o).spectrumAps(a), required_load_ap(a));

            end
            
            for u = 1 : length(users_per_ap)
                interference_pw = 0;
                for aa = 1 : deployment.nAps
                    if a ~= aa
                        interference_pw = interference_pw + db2pow(deployment.signalApSta(aa,users_per_ap(u)));
                    end
                end
                interference_dbm = pow2db(interference_pw);
                sinr = TX_POWER_DBM - pow2db(db2pow(interference_dbm) + db2pow(NOISE_DBM));
                % Compute the throughput according to spectrum available
                throughput_stas(users_per_ap(u)) = ...
                    allocated_bw_stas(u)*BANDWITDH_PER_CHANNEL * log2(1+sinr);
                spectrum_stas(users_per_ap(u)) = allocated_bw_stas(u);
            end
%             if ~isempty(users_per_ap)
%                 throughput_stas(users_per_ap)
%             end
            
        end
        
    end      
    
end

