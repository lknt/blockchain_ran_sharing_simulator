%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function max_distance = ComputeMaxDistance(deployment)
%ComputeMaxDistance computes the max distance allowed distance in order to
% allow a correct communication between an AP and an STA
% INPUT:
%   * deployment: object containing information about the deployment
% OUTPUT:
%   * max_distance: maximum allowed distance in meters

    PLd1=5;           % Path-loss factor
    shadowing = 9.5;  % Shadowing factor
    obstacles = 30;   % Obstacles factor
    alfa = 4.4;

    d = 0;
    delta_d = .1;
    while (true)        
        rx_power = deployment.TxPower - ...
            (PLd1 + 10 * alfa * log10(d) + ...
            shadowing / 2 + (d/10) .* obstacles / 2);
        
        if rx_power < deployment.ccaThreshold
            max_distance = d;
            break;
        else
            d = d + delta_d;
        end
    end   	

end