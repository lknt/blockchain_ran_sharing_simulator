%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [block_number, transaction_ix] = FindTransactionInBlockchain(blockchain, transaction_id)
%FindTransactionInBlockchain finds the index of a transaction in a blockchain
% INPUT:
%   * blockchain: Blockchain object
%   * transaction_id: identifier of the transaction of interest
% OUTPUT:
%   * block_number: index of the block containing the transaction
%   * transaction_ix: index of the transaction in the list

    for i = 1 : length(blockchain.mined_block_list)
        for j = 1 : length(blockchain.mined_block_list(i).transaction_list)
            if blockchain.mined_block_list(i).transaction_list(j).transaction_id == transaction_id
                block_number = i;
                transaction_ix = j;
            end
        end
    end

end