%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [ix_candidate_block, occurrences] = FindIndexCandidateBlock(block_list, miner_id, block_number)
%FindIndexCandidateBlock finds the index of a block in a list of blocks
% INPUT:
%   * block_list: list of block objects
%   * miner_id: identifier of the miner that mined the block of interest
%   * block_number: identifier of the block of interest
% OUTPUT:
%   * ix_candidate_block: index of the block in the list

    ix_candidate_block = -1;
    occurrences = [];
    for i = 1 : length(block_list)
        if (block_list(i).assigned_miner == miner_id || miner_id == -1) && ...
               block_list(i).block_id == block_number                   
            ix_candidate_block = i;
        elseif block_list(i).block_id == block_number && block_list(i).assigned_miner ~= miner_id
            if block_list(i).timestamp_mined > 0
                occurrences = [occurrences i];
            end            
        end
    end
        
end