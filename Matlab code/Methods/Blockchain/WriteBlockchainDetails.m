%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [] = WriteBlockchainDetails(LOGS_ENABLED, logs_file, miners)
%WriteBlockchainDetails writes blockchain details into a logs file
% INPUT:
%   * LOGS_ENABLED: flag indicating whether logs are enabled or not
%   * logs_file: file object to be written
%   * miners: miners object

    if LOGS_ENABLED
        load('constants.mat');
        WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL1 'Blockchain details:']);
        WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL2 'Miners'' information:']);
        WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Number of miners: ' num2str(length(miners))]);
        WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Miners'' details:']);
    %     for i = 1 : length(miners)
    %         WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL4 'Device id: ' num2str(miners(i).ap_id)]);
    %         WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL5 'Computational power: ' num2str(miners(i).computational_power)]);
    %         WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL5 'Associated devices: ' num2str(miners(i).associated_stas)]);
    %         WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL5 'Lambda (mining): ' num2str(miners(i).lambda_gen)]);
    %     end
    end

end