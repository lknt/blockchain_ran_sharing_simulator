%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function transaction_list = CheckTransactionsTobeRemoved(block, transaction_pool)
%CheckTransactionsTobeRemoved removes transactions from the pool once they
%have been added into a valid block
% INPUT:
%   * blockchain: Blockchain object
%   * transaction_pool: list of Transaction objects that are to be included in blocks
% OUTPUT:
%   * transaction_list: list of transactions to be removed from the pool

    % Get transactions that can be removed from the unc. pool
    transaction_list = [];
    transactions_to_remove = [];
    for i = 1 : block.num_transactions
        transactions_to_remove = [transactions_to_remove block.transaction_list(i).transaction_id];
    end
    % Check if those transactions can be removed
    if ~isempty(transactions_to_remove)
        transaction_list = [];
        removed_transactions = 0;
        for j = 1 : length(transaction_pool.transaction_list)
            for jj = 1 : length(transactions_to_remove)
                if transaction_pool.transaction_list(j).transaction_id == transactions_to_remove(jj) && ...
                        sum(find(transaction_list==transactions_to_remove(jj))) == 0
                    transaction_list = [transaction_list transactions_to_remove(jj)];
                    removed_transactions = removed_transactions + 1;
                end
            end
        end
    end    
        
end   