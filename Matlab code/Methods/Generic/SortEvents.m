%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function events = SortEvents(events)
%SortEvents sorts the list of events by time of arrival
% INPUT:
%   * events: unsorted events object
% OUTPUT:
%   * events: updated events object (sorted by time)
    
    [~,indexes] = sort(events.time);
    events.time = events.time(indexes);
    events.type = events.type(indexes);
    events.subtype = events.subtype(indexes);
    events.source_id = events.source_id(indexes);
    events.transaction_id = events.transaction_id(indexes);         
    events.service_ix = events.service_ix(indexes);  
          
end

