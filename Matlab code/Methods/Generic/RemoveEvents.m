%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function events = RemoveEvents(events, events_ids)
%RemoveEvents removes events
% INPUT:
%   * events: object containing events
%   * events_ids: identifiers of the events to be removed
% OUTPUT:
%   * events: updated object containing events
    
    events.time(events_ids) = [];  
    events.type(events_ids) = [];
    events.subtype(events_ids) = [];
    events.source_id(events_ids) = [];
    events.transaction_id(events_ids) = [];
    events.service_ix(events_ids) = [];
          
end

