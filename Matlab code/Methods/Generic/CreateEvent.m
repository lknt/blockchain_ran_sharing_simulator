%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function new_event = CreateEvent(type, subtype, time, source_id, transaction_id, service_ix)
%CreateEvent creates events objects to be used in the simulation
% INPUT:
%   * type: type of event
%   * subtype: subtype of event
%   * time: time at which the event is generated
%   * source_id: source of the event (e.g., UE, operator)
%   * transaction_id: identifier of the transaction associated to the event
%   * service_ix: index of the service requested/delivered associated to the event
% OUTPUT:
%   * new_event: event object
    
    new_event.type = type;
    new_event.subtype = subtype;
    new_event.time = time;
    new_event.source_id = source_id;
    new_event.transaction_id = transaction_id;
    new_event.service_ix = service_ix; 
          
end

