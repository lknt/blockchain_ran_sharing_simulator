function [] = WriteLogs(logs_flag, file_name, timestamp, message)
%WriteLogs method to write logs in a file
% INPUT:
%   * logs_flag: flag indicating whether logs are enabled (1) or not (0)
%   * file_name: name of the logs file to be written
%   * timestamp: timestamp of the simulation
%   * message: string message to be printed

if logs_flag
    fid = fopen(file_name, 'a');
    if fid == -1
      disp('Error: cannot open file for writing logs');
    end
    fprintf(fid, '%s; %.6f; %s\n', datestr(now, 0), timestamp, message);
    fclose(fid);
end

end