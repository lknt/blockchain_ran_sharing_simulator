%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function events = AddEvents(events, events_list)
%RemoveEvents removes events
% INPUT:
%   * events: object containing events
%   * events_ids: identifiers of the events to be removed
% OUTPUT:
%   * events: updated object containing events
    
    for i = 1 : length(events_list.time)
        events.time(end+1) = events_list(i).time;  
        events.type(end+1) = events_list(i).type;
        events.subtype(end+1) = events_list(i).subtype;
        events.source_id(end+1) = events_list(i).source_id;
        events.transaction_id(end+1) = events_list(i).transaction_id;
        events.service_ix(end+1) = events_list(i).service_ix;
    end
          
end

