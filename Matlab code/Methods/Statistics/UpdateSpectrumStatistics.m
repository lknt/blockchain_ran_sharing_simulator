%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [spectrum_ownership, spectrum_periods] = ...
    UpdateSpectrumStatistics(spectrum_ownership, spectrum_periods, operators, t)
%UpdateSpectrumStatistics updates the statistics related to spectrum sharing
% INPUT:
%   * spectrum_ownership: array with spectrum ownership ratios
%   * spectrum_periods: periods at which measurements are done
%   * operators: operators object
%   * t: time at which the measurement is done
% OUTPUT:
%   * spectrum_ownership: updated array with spectrum ownership ratios
%   * spectrum_periods: updated array with measurements periods

    % Compute and store the throughput until this moment
    for i = 1 : length(operators)
        spectrum_ownership{i}(end+1,:) = operators(i).spectrumAps;    
    end
    spectrum_periods(end+1) = t;
        
end