%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [operators] = UpdateSpectrumOwnership(operators, buyer_op, seller_op, ap_ix, spectrum_share)
%UpdateSpectrumNeeds updates the operators' object with the spectrum
%required by each operator, based on users' requirements and subscriptions
% INPUT:
%   * operators: object containing operators' information
%   * op1: 
%   * op2: 
%   * ap_ix: index of the involved AP
% OUTPUT:
%   * operators: updated operators object

    operators(buyer_op).spectrumAps(ap_ix) = operators(buyer_op).spectrumAps(ap_ix) + spectrum_share;
    operators(seller_op).spectrumAps(ap_ix) = operators(seller_op).spectrumAps(ap_ix) - spectrum_share;
          
end