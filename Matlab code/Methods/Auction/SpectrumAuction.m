%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [auction_winner, spectrum_share, service_duration] = ...
    SpectrumAuction(transaction, operators, ap_of_interest, auction_approach)
% INPUT:
%   * transaction: Transaction object with the transaction of interest
%   * operators: Operators object
%   * ap_of_interest: integer with the AP of interest (to carry out association)
%   * auction_approach: integer with the auction approach
% OUTPUT:
%   * auction_winner: integer with the winner of the auction (id of the operator)
%   * spectrum_share: float with the spectrum share provided to the user
%   * service_duration: float with the duration of the service provided to the user

    switch auction_approach
                
        case 1 % AUCTION_STATIC = 1;
            auction_winner = -1;
            spectrum_share = 0;
            service_duration = 0;
        
        case 2 % AUCTION_DYNAMIC = 2; 
            spectrum_available = zeros(1, length(operators));
            for i = 1 : length(operators)                
                if i ~= transaction.source_id
                    if find(operators(i).ownedAps == ap_of_interest)
                        spectrum_available(i) = max(0, ...
                            operators(i).spectrumAps(ap_of_interest) - ...
                            sum(operators(i).users_requirements{ap_of_interest}));
                    end                                          
                end                
            end  
            [val, ix_winner] = max(spectrum_available);
            if val > 0 % Sharing operator has resources to share
                auction_winner = ix_winner;
                spectrum_share = min(val, transaction.contract.offered_service(1));
                service_duration = transaction.contract.service_duration;
            else
                auction_winner = -1;
                spectrum_share = 0;
                service_duration = 0;
            end
                                    
        otherwise % Unknown type
            disp('Unknown auction type')
       
    end
    
end