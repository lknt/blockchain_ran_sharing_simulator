%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
function [] = WriteAuctionDetails(LOGS_ENABLED, logs_file, users,operators)

if LOGS_ENABLED
    load('constants.mat');
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL1 'Auction details:']);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL2 'Operators'' information:']);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Number of Operators: ' num2str(length(operators))]);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'APs/BSs owned by each operator:']);
    for i = 1 : length(operators)
        WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL4 'Operator ' num2str(i) ' (' num2str(length(operators(i).ownedAps)) ' APs)' ':']);
        WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL4 'AP id: ' num2str(operators(i).ownedAps)]);
        WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL4 'Channels per AP: ' ...
            num2str(operators(i).channelPerAp(find(operators(i).channelPerAp~=0)))]);
    end
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL2 'Users'' information:']);
    for i = 1 : length(users)
%        array_activation_prob = users(i).activationProbability;
        array_service_duration = users(i).averageServiceDuration;
        array_throughput_req = users(i).averageThroughputRequirement;
        array_delay_req = users(i).averageDelayRequirement;
    end
   % WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Average activation prob.: ' num2str(mean(array_activation_prob))]);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Average service duration: ' num2str(mean(array_service_duration)) ' seconds']);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Average throughput requirement: ' num2str(mean(array_throughput_req)) ' bps']);
    WriteLogs(LOGS_ENABLED, logs_file, 0, [LOGS_LVL3 'Average delay requirement: ' num2str(mean(array_delay_req)) ' seconds']);
end

end

