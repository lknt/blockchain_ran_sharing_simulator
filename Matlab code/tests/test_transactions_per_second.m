path_file = 'Output/output_4_2_5_5_3000_2_1.mat';
load(path_file);

% SERVICE BC
ts_transaction = [];
for i = 1 : length(service_bc.mined_block_list) 
    for j = 1 : length(service_bc.mined_block_list(i).transaction_list) 
        ts_transaction = [ts_transaction service_bc.mined_block_list(i).transaction_list(j).timestamp_created];        
    end
end
for i = 1 : length(service_bc.block_list) 
    for j = 1 : length(service_bc.block_list(i).transaction_list) 
        ts_transaction = [ts_transaction service_bc.block_list(i).transaction_list(j).timestamp_created];        
    end
end
sorted_transactions = sort(ts_transaction);

disp('SUMMARY SERVICE BC:')
disp([' + TPS: ' num2str(length(sorted_transactions)/sim_time)])

% SPECTRUM BC
ts_transaction = [];
for i = 1 : length(spectrum_bc.mined_block_list) 
    for j = 1 : length(spectrum_bc.mined_block_list(i).transaction_list) 
        ts_transaction = [ts_transaction spectrum_bc.mined_block_list(i).transaction_list(j).timestamp_created];        
    end
end
for i = 1 : length(spectrum_bc.block_list) 
    for j = 1 : length(spectrum_bc.block_list(i).transaction_list) 
        ts_transaction = [ts_transaction spectrum_bc.block_list(i).transaction_list(j).timestamp_created];        
    end
end
sorted_transactions = sort(ts_transaction);

disp('SUMMARY SERVICE BC:')
disp([' + TPS: ' num2str(length(sorted_transactions)/sim_time)])