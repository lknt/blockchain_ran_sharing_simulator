%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
classdef TransactionPool
    % Pool of transactions to be used by miners for generating blocks
    %   Contains transaction objects that are ready to be appended to the
    %   blockchain
    
    properties
        transaction_list
        total_size = 0
        transaction_count = 0
    end
    
    methods
        
        % Update the size of the unconfirmed pool
        function obj = UpdateSize(obj, TRANSACTION_LENGTH)
            %disp(['Updating pool size: ' num2str(length(obj.transaction_list)) '*' num2str(TRANSACTION_LENGTH) ]);
            obj.total_size = obj.total_size + TRANSACTION_LENGTH;
        end
        
        % Add transactions to unconfirmed pool
        function obj = AddTransaction(obj, new_transaction, is_confirmed, time, TRANSACTION_LENGTH, repeated)
            if ~isempty(obj.transaction_list)
                obj.transaction_list(end+1) = new_transaction;
            else
                obj.transaction_list = new_transaction;
            end
            
            obj.total_size = obj.total_size + TRANSACTION_LENGTH;           
            %if ~repeated, obj.transaction_count = obj.transaction_count + 1; end
            if is_confirmed, obj.transaction_list(end).timestamp_confirmed = time; end
        end
        
        % Remove transactions from unconfirmed pool
        function obj = RemoveTransactions(obj, transactions_to_remove, TRANSACTION_LENGTH)
            aux_transactions_to_remove = transactions_to_remove;
            for i = length(obj.transaction_list) : -1 : 1
                for j = 1 : length(aux_transactions_to_remove)
                    if obj.transaction_list(i).transaction_id == aux_transactions_to_remove(j)
                        obj.transaction_list(i) = [];   
                        aux_transactions_to_remove(j) = [];
                        break
                    end
                end
            end
            obj.total_size = obj.total_size - length(transactions_to_remove)*TRANSACTION_LENGTH;
        end
        
    end

end

