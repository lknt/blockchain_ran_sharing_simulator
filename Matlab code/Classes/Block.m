%%% *********************************************************************
%%% * Blockchain-Enabled RAN Sharing for Future 5G/6G Communications    *
%%% * Authors: Lorenza Giupponi & Francesc Wilhelmi (fwilhelmi@cttc.cat)*
%%% * Copyright (C) 2020-2025, and GNU GPLd, by Francesc Wilhelmi       *
%%% * GitHub repository: ...                                            *
%%% *********************************************************************
classdef Block
    % Block object to be used in a blockchain
    %   The block object contains the information of a
    %   confirmed/unconfirmed block to be appended in a blockchain
       
    properties
        block_id
        is_mined
        assigned_miner
        timestamp_created
        time_to_mine
        timestamp_mined
        transaction_list
        num_transactions
        nonce
        hash
        previous_hash
        fork
    end
    
    methods(Static)
        function [] = PrintBlockInfo()
            load('constants.mat')
            disp(['Block ' num2str(obj.block_id) ' information:'])
            disp([LOG_LVL1 'Is mined: ' num2str(obj.is_mined)])
            disp([LOG_LVL1 'Timestamp created: ' num2str(obj.timestamp_created)])
            disp([LOG_LVL1 'Timestamp mined: ' num2str(obj.timestamp_mined)])
            disp([LOG_LVL1 'Number of transactions: ' num2str(length(obj.transaction_list))])
            disp([LOG_LVL1 'transactions_ids: '])
            for i = 1 : length(obj.transaction_list)
                disp([obj.transaction_list(i).transaction_id ' '])
            end
            disp([LOG_LVL1 'nonce: ' obj.nonce])
            disp([LOG_LVL1 'hash: ' obj.hash])
            disp([LOG_LVL1 'previous_hash: ' obj.previous_hash])
        end
    end
    
end

