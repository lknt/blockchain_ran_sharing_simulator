% Path where the results are stored
path_folder = '1_two_mno_1_0/1_0/';
% Parameters of interest for plotting
lambda_boss = [10];
block_size_boss = 5;
% Process output results from "path_folder"
for a = 4 : 4
    for aa = 1 : 2     
        for l = 1 : 1
            for ti = 1 : 1               
                for s = 1 : 1
                    BLOCK_SIZE = TRANSACTION_LENGTH.*block_size_boss(s);
                    % Load results from simulations and prepare data to be plotted
                    load(['./output/' path_folder '/output_' num2str(a) '_' ...
                        num2str(aa) '_' num2str(lambda_boss(l)) '_' ...
                        num2str(block_timeout(ti)) '_' num2str(BLOCK_SIZE) '_2_1.mat'])
                    throughput_requested{aa} = services_information(:,3);
                    start_service{aa} = services_information(:,8);
                    finish_service{aa} = services_information(:,9);     
                    throughput_per_user{aa} = throughput_users;
                    satisfaction_per_user{aa} = satisfaction_users;
                    activation_user{aa} = activation_users;
                   	requests_user{aa} = requests_users;
                    durations{aa} = load_aps;                    
                end
            end
        end
        
        % Process results considering active users
        for i = 1 : length(period_durations)
            throughput_in_period = throughput_users(i,:);
            satisfaction_in_period = satisfaction_users(i,:);
            requests_in_period = requests_users;
            ixes_active_users = find(activation_users(i,:)>0);
            if ~isempty(ixes_active_users)
                throughput_active_users = throughput_in_period(ixes_active_users);
                satisfaction_active_users = satisfaction_in_period(ixes_active_users);
                bw_requested_active_users = requests_in_period(ixes_active_users);
                %
                agg_tpt{aa}(i) = sum(throughput_active_users);
                mean_tpt{aa}(i) = mean(throughput_active_users);
                mean_satisfaction{aa}(i) = mean(satisfaction_active_users);
                agg_bw_requested{aa}(i) = sum(bw_requested_active_users);
                mean_bw_requested{aa}(i) = mean(bw_requested_active_users);
                mean_load_aps{aa}(i) = mean(load_aps(i,:));
                mean_required_load_aps{aa}(i) = mean(required_load_aps(i,:));
            else
                agg_tpt{aa}(i) = 0;
                mean_tpt{aa}(i) = 0;
                mean_satisfaction{aa}(i) = 0;
                agg_bw_requested{aa}(i) = 0;
                mean_bw_requested{aa}(i) = 0;
                mean_load_aps{aa}(i) = 0;
                mean_required_load_aps{aa}(i) = 0;
            end
        end        
        
        % Distribute results over time periods of the same length
        nSamples = 100;
        delta_t = (sim_time/nSamples);
        cumulative_time = cumsum(period_durations);
        for t = 1 : nSamples
            samples_period = [];
            for i = 1 : length(cumulative_time)
                if ((t*delta_t)-delta_t < cumulative_time(i) && cumulative_time(i) <= (t*delta_t)) %|| ...
                    samples_period = [samples_period i];
                end
            end
            if isempty(samples_period)
                aggregate_ue_perf{aa}(t) = aggregate_ue_perf{aa}(t-1);
                mean_ue_perf{aa}(t) = mean_ue_perf{aa}(t-1);
                mean_ue_satisfaction{aa}(t) = mean_ue_satisfaction{aa}(t-1);
                aggregate_bandwidth_required{aa}(t) = aggregate_bandwidth_required{aa}(t-1);
                mean_bandwidth_required{aa}(t) = mean_bandwidth_required{aa}(t-1);
                mean_load_operators{aa}(t) = mean_load_operators{aa}(t-1);
                total_traffic_load_users{aa}(t) = total_traffic_load_users{aa}(t-1);
            else
                aggregate_ue_perf{aa}(t) = mean(agg_tpt{aa}(samples_period));
                mean_ue_perf{aa}(t) = mean(mean_tpt{aa}(samples_period));
                mean_ue_satisfaction{aa}(t) = mean(mean_satisfaction{aa}(samples_period));
                aggregate_bandwidth_required{aa}(t) = mean(agg_bw_requested{aa}(samples_period));
                mean_bandwidth_required{aa}(t) = mean(mean_bw_requested{aa}(samples_period));
                mean_load_operators{aa}(t) = min(1,mean(mean_required_load_aps{aa}(samples_period)));
                total_traffic_load_users{aa}(t) = sum((agg_tpt{aa}(samples_period)))*delta_t;
            end
        end

    end
end

%% Plot of the temporal agg. capacity and service acceptance
fig = figure;

subplot(1,2,1)
plot(aggregate_ue_perf{1}/1e6, 'b-s', 'linewidth', 2.0, 'MarkerSize', 8, ...
    'MarkerIndices', 1:5:length(aggregate_ue_perf{1}))
hold on
axis([0 nSamples 0 140])
xlabel('Time (s)')
ylabel('Capacity (Mbps)')
yyaxis right
plot(mean_ue_satisfaction{1}, 'r-o', 'linewidth', 1.0, 'MarkerSize', 8, ...
    'MarkerIndices', 1:5:length(mean_satisfaction{2}))
hold on
plot(mean_load_operators{1}, 'r--x', 'linewidth', 1.0, 'MarkerSize', 8, ...
    'MarkerIndices', 1:5:length(mean_load_operators{1})) 
axis([0 nSamples 0 1])
grid on
grid minor
set(gca,'FontSize',18,'FontName','Times')
ylabel('Satisfaction / Load')
legend({'Agg. UE capacity', 'Mean UE satisfaction', 'Mean BS load'})

subplot(1,2,2)
plot(aggregate_ue_perf{2}/1e6, 'b-s', 'linewidth', 2.0, 'MarkerSize', 8, ...
    'MarkerIndices', 1:5:length(aggregate_ue_perf{2}))
hold on
axis([0 nSamples 0 140])
xlabel('Time (s)')
ylabel('Capacity (Mbps)')
yyaxis right
plot(mean_satisfaction{2}, 'r-o', 'linewidth', 1.0, 'MarkerSize', 8, ...
    'MarkerIndices', 1:5:length(mean_satisfaction{2}))
hold on
plot(mean_load_operators{2}, 'r--x', 'linewidth', 1.0, 'MarkerSize', 8, ...
    'MarkerIndices', 1:5:length(mean_load_operators{2})) 
axis([0 nSamples 0 1])
grid on
grid minor
set(gca,'FontSize',18,'FontName','Times')
ylabel('Satisfaction / Load')
legend({'Agg. UE capacity', 'Mean UE satisfaction', 'Mean BS load'})

save_figure( fig, './output/performance_satisfaction', '' )