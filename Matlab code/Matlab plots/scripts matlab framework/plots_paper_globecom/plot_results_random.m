clc

path_folder = '2_random_deployments/1_two_mno/ratio_random/';
mean_agg_tpt = cell(1,2);
num_deployments = 10;
block_size = 5;
BLOCK_SIZE = TRANSACTION_LENGTH.*block_size(1);
lambda_master = [10 15 20];%[1 2.5 5 7.5 10 12.5 15];

for aa = 1 : 2%length(spectrum_auction_modes)         
    average_agg_tpt = zeros(length(lambda_master),1);
    average_mean_tpt = zeros(length(lambda_master),1);
    average_mean_satisfaction = zeros(length(lambda_master),1);
    average_mean_load_aps = zeros(length(lambda_master),1);   
    
    total_tpt = cell(3,4);
    total_satisfaction = cell(3,4);
    
    for o = 1 : 4          
        for l = 1 : length(lambda_master)            
            total_tpt{l,o} = [];  
            new_agg_tpt = zeros(1,num_deployments);
            new_mean_tpt = zeros(1,num_deployments);
            for k = 1 : num_deployments
                BLOCK_SIZE = TRANSACTION_LENGTH.*block_size(1);
                % Load results from simulations and prepare data to be plotted
                load(['./output/' path_folder '/output_4_' ...
                    num2str(aa) '_' num2str(lambda_master(l)) '_' ...
                    num2str(5) '_' num2str(BLOCK_SIZE) '_' num2str(o+1) '_' num2str(k) '.mat'])
                % Process sim. results
                agg_tpt = zeros(1,length(period_durations));
                mean_tpt = zeros(1,length(period_durations));
                mean_satisfaction = zeros(1,length(period_durations));
                mean_load_aps = zeros(1,length(period_durations));
                for i = 1 : length(period_durations)
                    throughput_in_period = throughput_users(i,:);
                    satisfaction_in_period = satisfaction_users(i,:);
                    requests_in_period = requests_users(i,:);
                    ixes_active_users = find(activation_users(i,:)>0);
                    if ~isempty(ixes_active_users)
                        throughput_active_users = throughput_in_period(ixes_active_users);
                        satisfaction_active_users = satisfaction_in_period(ixes_active_users);
                        agg_tpt(i) = sum(throughput_active_users);
                        mean_tpt(i) = mean(throughput_active_users);
                        mean_satisfaction(i) = mean(satisfaction_active_users);
                        mean_load_aps(i) = mean(load_aps(i,:));
                    else
                        agg_tpt(i) = 0;
                        mean_tpt(i) = 0;
                        mean_satisfaction(i) = 0;
                        mean_load_aps(i) = 0;
                    end
                end
                ixes_valid_periods = [];
                for i = 1 : length(period_durations)                    
                    ixes_active_users = find(activation_users(i,:)>0);
                    if ~isempty(ixes_active_users) && period_durations(i) > 0
                        ixes_valid_periods = [ixes_valid_periods i];
                    end
                end
                averaged_tpt = sum( throughput_users(ixes_valid_periods,:) .* ...
                    period_durations(ixes_valid_periods)' / sum(period_durations(ixes_valid_periods)) );
                
                new_agg_tpt(k) = sum(averaged_tpt);
                new_mean_tpt(k) = mean(averaged_tpt);
                             
                % Gather results for each random depl.
                temporal_agg_tpt(k) = mean(agg_tpt);
                temporal_mean_tpt(k) = mean(mean_tpt);
                temporal_mean_satisfaction(k) = mean(mean_satisfaction);
                temporal_mean_load_aps(k) = mean(mean_load_aps);     
                
                all_valid_tpt_measurements = throughput_users(ixes_valid_periods,:);
                all_valid_satisfaction_measurements = satisfaction_users(ixes_valid_periods,:);
                
                total_tpt{l,o} = [total_tpt{l,o}; all_valid_tpt_measurements(:)];
                total_satisfaction{l,o} = [total_satisfaction{l,o}; all_valid_satisfaction_measurements(:)];
                
            end % k = 1 : num_deployments 

%             disp([' - Mean agg. throughput per deployment: ' num2str(new_agg_tpt)])
%             disp([' - Mean avg. throughput per deployment: ' num2str(new_mean_tpt)])
%             disp([' - Mean avg. satisfaction per deployment: ' num2str(temporal_mean_satisfaction)])
%             disp([' - Mean avg. load per deployment: ' num2str(temporal_mean_load_aps)])
            
            average_agg_tpt(l,o) = mean(new_agg_tpt);
            std_agg_tpt(l,o) = std(new_agg_tpt);
            average_mean_tpt(l,o) = mean(new_mean_tpt);
            std_mean_tpt(l,o) = std(new_mean_tpt);
            
%             average_agg_tpt(l,o-1) = mean(temporal_agg_tpt);
%             std_agg_tpt(l,o-1) = std(temporal_agg_tpt);
%             average_mean_tpt(l,o-1) = mean(temporal_mean_tpt);
%             std_mean_tpt(l,o-1) = std(temporal_mean_tpt);
            average_mean_satisfaction(l,o) = mean(temporal_mean_satisfaction);
            std_mean_satisfaction(l,o) = std(temporal_mean_satisfaction);
            average_mean_load_aps(l,o) = mean(temporal_mean_load_aps);
            std_mean_load_aps(l,o) = std(temporal_mean_load_aps);
%                     
        end % l = 1 : length(lambda)
    
    save(['tmp/average_agg_tpt' num2str(aa) '_' num2str(o)],'average_agg_tpt')
    save(['tmp/average_mean_tpt' num2str(aa) '_' num2str(o)],'average_mean_tpt')
    save(['tmp/average_mean_satisfaction' num2str(aa) '_' num2str(o)],'average_mean_satisfaction')
    save(['tmp/average_mean_load_aps' num2str(aa) '_' num2str(o)],'average_mean_load_aps')
    save(['tmp/std_agg_tpt' num2str(aa) '_' num2str(o)],'std_agg_tpt')
    save(['tmp/std_mean_tpt' num2str(aa) '_' num2str(o)],'std_mean_tpt')
    save(['tmp/std_mean_satisfaction' num2str(aa) '_' num2str(o)],'std_mean_satisfaction')
    save(['tmp/std_mean_load_aps' num2str(aa) '_' num2str(o)],'std_mean_load_aps')
    end % o = 2 : 2 

    save(['tmp/total_tpt' num2str(aa)],'total_tpt')
    save(['tmp/total_satisfaction' num2str(aa)],'total_satisfaction')
    
end % aa = 1 : length(spectrum_auction_modes) 

%% Boxplot throughput
figure
for i = 1 : 2
    subplot(1,2,i)
    x = [];
    g = [];
    load(['tmp/total_tpt' num2str(i)])
    for o = 1 : 4
        x = [x; total_tpt{2,o}];
        g = [g; (o-1)*ones(length(total_tpt{2,o}), 1)];
    end
    boxplot(x,g)
    grid on
    grid minor
    set(gca,'FontSize',18,'FontName','Times')
end

%% Boxplot satisfaction
figure
for i = 1 : 2
    subplot(1,2,i)
    x = [];
    g = [];
    load(['tmp/total_satisfaction' num2str(i)])
    for o = 1 : 4
        x = [x; total_satisfaction{2,o}];
        g = [g; (o-1)*ones(length(total_satisfaction{2,o}), 1)];
    end
    boxplot(x,g)
    grid on
    grid minor
    set(gca,'FontSize',18,'FontName','Times')
end

%% Agg tpt
mean_throughput = cell(2,4);
std_throughput = cell(2,4);
for i = 1 : 2
    for o = 1 : 4
        load(['tmp/average_agg_tpt' num2str(i) '_' num2str(o)])
        for l = 1 : length(lambda_master)
            mean_throughput{i,o}(l) = mean(average_agg_tpt(l,:));
            std_throughput{i,o}(l) = mean(std_agg_tpt(l,:)); 
        end
    end
end
figure
subplot(1,2,1)
b1 = [mean_throughput{1,1}'./1e6 mean_throughput{1,2}'./1e6 ...
    mean_throughput{1,3}'./1e6 mean_throughput{1,4}'./1e6];
b11 = [std_throughput{1,1}'./1e6 std_throughput{1,2}'./1e6 ...
    std_throughput{1,3}'./1e6 std_throughput{1,4}'./1e6];
bar(b1);
hold on
grid on
grid minor
set(gca,'FontSize',18,'FontName','Times')
xlabel('User profile')
ylabel('Agg. capacity (Mbps)')
axis([0 4 0 300])
ngroups = size(b1, 1);
nbars = size(b1, 2);
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x, b1(:,i), b11(:,i), '.');
end
subplot(1,2,2)
b2 = [mean_throughput{2,1}'./1e6 mean_throughput{2,2}'./1e6 ...
    mean_throughput{2,3}'./1e6 mean_throughput{2,4}'./1e6];
b22 = [std_throughput{2,1}'./1e6 std_throughput{2,2}'./1e6 ...
    std_throughput{2,3}'./1e6 std_throughput{2,4}'./1e6];
bar(b2);%,  'FaceAlpha',0.2, 'LineStyle', '--');
hold on
ngroups = size(b2, 1);
nbars = size(b2, 2);
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x, b2(:,i), b22(:,i), '.');
end
grid on
grid minor
set(gca,'FontSize',18,'FontName','Times')
xlabel('User profile')
ylabel('Agg. capacity (Mbps)')
legend({'M = 2', 'M = 3', 'M = 4', 'M = 5'})
axis([0 4 0 300])

%% Service acceptance
mean_satisfaction = cell(2,4);
std_satisfaction = cell(2,4);
for i = 1 : 2
    for o = 1 : 4
        load(['tmp/average_mean_satisfaction' num2str(i) '_' num2str(o)])
        load(['tmp/std_mean_satisfaction' num2str(i) '_' num2str(o)])
        for l = 1 : length(lambda_master)
            mean_satisfaction{i,o}(l) = mean(average_mean_satisfaction(l,:));
            std_satisfaction{i,o}(l) = mean(std_mean_satisfaction(l,:)); 
        end
    end
end
figure
subplot(1,2,1)
b1 = [mean_satisfaction{1,1}' mean_satisfaction{1,2}' ...
    mean_satisfaction{1,3}' mean_satisfaction{1,4}'];
b11 = [std_satisfaction{1,1}' std_satisfaction{1,2}' ...
    std_satisfaction{1,3}' std_satisfaction{1,4}'];
bar(b1);
hold on
ngroups = size(b1, 1);
nbars = size(b1, 2);
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x, b1(:,i), b11(:,i), '.');
end
grid on
grid minor
set(gca,'FontSize',18,'FontName','Times')
xlabel('User profile')
ylabel('Service acceptance')
axis([0 4 0 1.1])
subplot(1,2,2)
b2 = [mean_satisfaction{2,1}' mean_satisfaction{2,2}' ...
    mean_satisfaction{2,3}' mean_satisfaction{2,4}'];
b22 = [std_satisfaction{2,1}' std_satisfaction{2,2}' ...
    std_satisfaction{2,3}' std_satisfaction{2,4}'];
bar(b2);%,  'FaceAlpha',0.2, 'LineStyle', '--');
hold on
ngroups = size(b2, 1);
nbars = size(b2, 2);
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
for i = 1:nbars
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x, b2(:,i), b22(:,i), '.');
end
grid on
grid minor
set(gca,'FontSize',18,'FontName','Times')
xlabel('User profile')
ylabel('Service acceptance')
axis([0 4 0 1.1])
legend({'M = 2', 'M = 3', 'M = 4', 'M = 5'})

%% LOAD APs
mean_load = cell(2,4);
std_load = cell(2,4);
for i = 1 : 2
    for o = 1 : 4
        load(['tmp/average_mean_load_aps' num2str(i) '_' num2str(o)])
        for l = 1 : length(lambda_master)
            mean_load{i,o}(l) = mean(average_mean_load_aps(l,:));
            std_load{i,o}(l) = mean(std_mean_load_aps(l,:)); 
        end
    end
end
figure
b1 = [mean_load{1,1}' mean_load{1,2}' ...
    mean_load{1,3}' mean_load{1,4}'];
bar(b1);
hold on
b2 = [mean_load{2,1}' mean_load{2,2}' ...
    mean_load{2,3}' mean_load{2,4}'];
bar(b2, 'FaceAlpha',0.2, 'LineStyle', '--');
grid on
grid minor
set(gca,'FontSize',18,'FontName','Times')
xlabel('\lambda')
ylabel('Mean load BSs')
legend({'Num. MNOs = 2', 'Num. MNOs = 3', 'Num. MNOs = 4', 'Num. MNOs = 5'})

%% 
% figure
% mean_user_satisfaction = zeros(2,length(lambda_master));
% std_user_satisfaction = zeros(2,length(lambda_master));
% for i = 1 : 2
%     load(['tmp/average_mean_satisfaction' num2str(i)])
%     for l = 1 : length(lambda_master)
%         mean_user_satisfaction(i,l) = mean(mean(average_mean_satisfaction(l,:)));
%         std_user_satisfaction(i,l) = std(mean(average_mean_satisfaction(l,:))); 
%     end
% end
% bar(mean_user_satisfaction');
% title('User acceptance')
% grid on
% grid minor
% set(gca,'FontSize',18,'FontName','Times')
% xlabel('User arrivals')
% ylabel('User acceptance')
% legend({'Static', 'Dynamic'})
% 
% figure
% mean_load = zeros(2,length(lambda_master));
% std_load = zeros(2,length(lambda_master));
% for i = 1 : 2
%     load(['tmp/average_mean_load_aps' num2str(i)])
%     for l = 1 : length(lambda_master)
%         mean_load(i,l) = mean(mean(average_mean_load_aps(l,:)));
%         std_load(i,l) = std(mean(average_mean_load_aps(l,:))); 
%     end
% end
% bar(mean_load');
% title('BS Load')
% grid on
% grid minor
% set(gca,'FontSize',18,'FontName','Times')
% xlabel('User arrivals')
% ylabel('Load')
% legend({'Static', 'Dynamic'})

%hold on
%errorbar(mean_throughput, std_throughput);

% boxplot([all_res2])
% hold on 
% plot(aggregate_ue_perf{1}/1e6, 'b-s', 'linewidth', 2.0, 'MarkerSize', 8, ...
%     'MarkerIndices', 1:5:length(aggregate_ue_perf{1}))
% hold on
% axis([0 nSamples 0 1.1*max(aggregate_ue_perf{2}/1e6)])
% xlabel('Time (s)')
% ylabel('Capacity (Mbps)')
% yyaxis right
% plot(mean_ue_satisfaction{1}, 'r-o', 'linewidth', 1.0, 'MarkerSize', 8, ...
%     'MarkerIndices', 1:5:length(mean_satisfaction{2}))
% hold on
% plot(mean_load_operators{1}, 'r--x', 'linewidth', 1.0, 'MarkerSize', 8, ...
%     'MarkerIndices', 1:5:length(mean_load_operators{1})) 
% axis([0 nSamples 0 1])
% %plot(ones(1, nSamples), 'k--', 'linewidth', 1.0)
% grid on
% grid minor
% set(gca,'FontSize',18,'FontName','Times')
% ylabel('Satisfaction / Load')
% legend({'Agg. UE capacity', 'Mean UE satisfaction', 'Mean BS load'})
% % 
% subplot(1,2,2)
% plot(aggregate_ue_perf{2}/1e6, 'b-s', 'linewidth', 2.0, 'MarkerSize', 8, ...
%     'MarkerIndices', 1:5:length(aggregate_ue_perf{2}))
% hold on
% axis([0 nSamples 0 1.1*max(aggregate_ue_perf{2}/1e6)])
% xlabel('Time (s)')
% ylabel('Capacity (Mbps)')
% yyaxis right
% plot(mean_satisfaction{2}, 'r-o', 'linewidth', 1.0, 'MarkerSize', 8, ...
%     'MarkerIndices', 1:5:length(mean_satisfaction{2}))
% hold on
% plot(mean_load_operators{2}, 'r--x', 'linewidth', 1.0, 'MarkerSize', 8, ...
%     'MarkerIndices', 1:5:length(mean_load_operators{2})) 
% axis([0 nSamples 0 1])
% grid on
% grid minor
% set(gca,'FontSize',18,'FontName','Times')
% ylabel('Satisfaction / Load')
% legend({'Agg. UE capacity', 'Mean UE satisfaction', 'Mean BS load'})
% 
% 
% % %% Boxplot total delay
% % titles = {'\lambda=5','\lambda=10','\lambda=15'};
% % fig = figure;
% % for aa = 1:3
% % subplot(1,3,aa)
% % data = [];
% % grp = [];
% % for s = 1 : length(block_size)
% %     data = [data; total_delay_fw{aa,s}(total_delay_fw{aa,s}>0)];
% %     grp = [grp (s-1)*ones(1,length(total_delay_fw{aa,s}(total_delay_fw{aa,s}>0)))];
% %     mean_total_delay(s) = mean(total_delay_fw{aa,s}(total_delay_fw{aa,s}>0));
% % end
% % boxplot(data,grp)
% % hold on
% % axis([0 6 0 1.4])
% % % title(['Block timeout = ' num2str(block_timeout(k))])
% % xlabel('Block size (#blocks)')
% % ylabel('Total delay (s)')
% % xticks(1:length(block_size))
% % xticklabels(1:length(block_size))
% % title(titles{aa})
% % grid on
% % grid minor
% % set(gca,'FontSize',16,'FontName','Times')
% % end
% 
% %save_figure( fig, './output/delay_blockchain_bxplot_total', '' )