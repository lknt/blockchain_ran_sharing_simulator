% load('overhead_01')
% oh01 = overhead;
% load('overhead_5')
% oh5 = overhead;

load('overhead_service_bc_1')
oh01 = overhead_service_bc;
load('overhead_service_bc_5')
oh5 = overhead_service_bc;

for l = 1 : 3
    for s = 1 : 10
        mean_oh1(l,s) = mean(oh01{l,s})/300e6;
        mean_oh5(l,s) = mean(oh5{l,s})/300e6;
    end
%     subplot(1,3,l)
%     bar([mean_oh1(l,:)' mean_oh5(l,:)'])
%     axis([0 11 0 1.1])
end

%%
bar([mean(mean_oh1'); mean(mean_oh5')])
grid on
grid minor
xlabel('T_{wait} (s)')
ylabel('Overhead (Mbps)')
set(gca,'FontSize',18,'FontName','Times')
legend({'\lambda = 1', '\lambda = 5', '\lambda = 10'})
xticks(1:2)
xticklabels({'0.1', '5'})
%%


%% Overheads of the service BC for lambda = {0.1, 5}
figure
subplot(1,3,1)
bar([oh01(1,:)'./300e6 oh5(1,:)'./300e6], 'stacked');
axis([0 11 0 2.4])
set(gca,'FontSize',18,'FontName','Times')
title('\lambda = 1')
grid on
grid minor
xlabel('Block size (# trans.)')
ylabel('Overhead (bps)')
legend({'T_w = 0.1s', 'T_w = 5s'})
subplot(1,3,2)
bar([oh01(2,:)'./300e6 oh5(2,:)'./300e6], 'stacked');
axis([0 11 0 2.4])
set(gca,'FontSize',18,'FontName','Times')
title('\lambda = 5')
grid on
grid minor
xlabel('Block size (# trans.)')
ylabel('Overhead (bps)')
legend({'T_w = 0.1s', 'T_w = 5s'})
subplot(1,3,3)
bar([oh01(3,:)'./300e6 oh5(3,:)'./300e6], 'stacked');
axis([0 11 0 2.4])
set(gca,'FontSize',18,'FontName','Times')
title('\lambda = 10')
grid on
grid minor
xlabel('Block size (# trans.)')
ylabel('Overhead (bps)')
legend({'T_w = 0.1s', 'T_w = 5s'})