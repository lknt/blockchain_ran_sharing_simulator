% Parameters of interest for plotting
service_auction_modes = [SELECTION_RANDOM];
spectrum_auction_modes = [LEASE_AUCTION];
block_timeout = [01];        % timeout in seconds for generating a block
block_size_master = 1:10;   % block size in number of transactions
queue_length = 1000;        % number of transactions that fit the queue
lambda_master = [1 5 10];   % arrivals rate (UE requests)
num_deployments = 1;
% Path where the results are stored
path_folder = ['3_blockchain_performance/timer_' num2str(block_timeout) '/with_forks'];
% Process output results from "path_folder"
for a = 1 : length(service_auction_modes)
    for aa = 1 : length(spectrum_auction_modes)   
        for l = 1 : length(lambda_master)
            for ti = 1 : length(block_timeout)               
                for s = 1 : length(block_size_master)
                    for k = 1 : num_deployments
                        BLOCK_SIZE = TRANSACTION_LENGTH.*block_size_master(s);
                        % Load results from simulations and prepare data to be plotted
                        load(['./output/' path_folder '/output_' num2str(service_auction_modes(a)) '_' ...
                            num2str(spectrum_auction_modes(aa)) '_' num2str(lambda_master(l)) '_' ...
                            num2str(block_timeout(ti)) '_' num2str(BLOCK_SIZE) '_2_1.mat'])
                        % Service BC
                        up_d_service{l,s} = services_information(:,5) - services_information(:,4);
                        q_d_service{l,s} = services_information(:,7) - services_information(:,5);
                        mine_d_service{l,s} = services_information(:,7) - services_information(:,6);
                        total_d_service{l,s} = services_information(:,8) - services_information(:,4);  
                        fork_prob(l,s) = n_forks/length(service_bc.mined_block_list);
                        % Spectrum BC
                        up_d_spectrum{l,s} = spectrum_information(:,6) - spectrum_information(:,5);
                        q_d_spectrum{l,s} = spectrum_information(:,8) - spectrum_information(:,6);
                        mine_d_spectrum{l,s} = spectrum_information(:,8) - spectrum_information(:,7);
                        total_d_spectrum{l,s} = spectrum_information(:,9) - spectrum_information(:,5);  
                        % Overhead service blockchain
                        nSamples = 1000;
                        step = sim_time/nSamples;
                        traffic_users{l,s} = zeros(1,nSamples);
                        overhead_service_bc{l,s} = zeros(1,nSamples);
                        time_measurements = cumsum(period_durations);    
                        for t = 1 : nSamples
                            for j = 1 : length(timestamp_bc)
                                if (t-1)*step < timestamp_bc(j) && t*step >= timestamp_bc(j)          
                                    overhead_service_bc{l,s}(t) = ...
                                        overhead_service_bc{l,s}(t) + oh_an(j) + oh_p2p(j);
                                end
                            end
                            for j = 1 : length(time_measurements)
                                if (t-1)*step < time_measurements(j) && ...
                                       t*step >= time_measurements(j)        
                                    traffic_users{l,s}(t) = traffic_users{l,s}(t) + sum(throughput_users(j,:))*period_durations(j);
                                end
                            end
                        end 
%                         % Overhead spectrum blockchain
%                         nSamples = 1000;
%                         step = sim_time/nSamples;
%                         overhead_spectrum_bc{l,s} = zeros(1,nSamples);
%                         time_measurements = cumsum(period_durations);    
%                         for t = 1 : nSamples
%                             for j = 1 : length(timestamp_bc)
%                                 if (t-1)*step < timestamp_bc(j) && t*step >= timestamp_bc(j)          
%                                     overhead_spectrum_bc{l,s}(t) = ...
%                                         overhead_spectrum_bc{l,s}(t) + oh_an2(j) + oh_p2p2(j);
%                                 end
%                             end
%                         end 
                    end
                end
            end
        end
    end
end

save(['overhead_service_bc_' num2str(block_timeout)],'overhead_service_bc')
%save(['overhead_spectrum_bc_' num2str(block_timeout)],'overhead_spectrum_bc')

%% Boxplot transaction confirmation delay of the Service BC
titles = {'\lambda=5','\lambda=10','\lambda=15'};
fig = figure;
for l = 1:length(lambda_master)
subplot(1,length(lambda_master),l)
data = [];
grp = [];
for s = 1 : length(block_size_master)
    data = [data; total_d_service{l,s}(total_d_service{l,s}>0)];
    grp = [grp (s-1)*ones(1,length(total_d_service{l,s}(total_d_service{l,s}>0)))];
    mean_total_delay(s) = mean(total_d_service{l,s}(total_d_service{l,s}>0));
end
boxplot(data,grp)
hold on
axis([0 11 0 25])
% title(['Block timeout = ' num2str(block_timeout(k))])
xlabel('Block size (#trans.)')
ylabel('Total delay (s)')
xticks(1:length(block_size_master))
xticklabels(1:length(block_size_master))
title(titles{l})
grid on
grid minor
set(gca,'FontSize',16,'FontName','Times')
end

%% Boxplot transaction confirmation delay of the Spectrum BC
titles = {'\lambda=5','\lambda=10','\lambda=15'};
fig = figure;
for l = 1:length(lambda_master)
subplot(1,length(lambda_master),l)
data = [];
grp = [];
for s = 1 : length(block_size_master)
    data = [data; total_d_spectrum{l,s}(total_d_spectrum{l,s}>0)];
    grp = [grp (s-1)*ones(1,length(total_d_spectrum{l,s}(total_d_spectrum{l,s}>0)))];
    mean_total_delay(s) = mean(total_d_spectrum{l,s}(total_d_spectrum{l,s}>0));
end
boxplot(data,grp)
hold on
axis([0 11 0 4])
% title(['Block timeout = ' num2str(block_timeout(k))])
xlabel('Block size (#trans.)')
ylabel('Total delay (s)')
xticks(1:length(block_size_master))
xticklabels(1:length(block_size_master))
title(titles{l})
grid on
grid minor
set(gca,'FontSize',16,'FontName','Times')
end

%% Bar plot of the transaction conf. delay for different lambda and block_size
fig = figure;
for l = 1:length(lambda_master)
    for s = 1 : length(block_size_master)
        mean_upload_delay(l,s) = mean(up_d_spectrum{l,s}(up_d_spectrum{l,s}>0));
        mean_queue_delay(l,s) = mean(q_d_spectrum{l,s}(q_d_spectrum{l,s}>0));
        mean_mining_delay(l,s) = mean(mine_d_spectrum{l,s}(mine_d_spectrum{l,s}>0));
        mean_total_delay1(l,s) = mean(total_d_service{l,s}(total_d_service{l,s}>0));     
        mean_total_delay2(l,s) = mean(total_d_spectrum{l,s}(total_d_spectrum{l,s}>0));        
    end  
end 
for l = 1 : length(lambda_master)
    subplot(1,length(lambda_master),l)
    bar([mean_total_delay1(l,:)' mean_total_delay2(l,:)'], 'stacked');
    ylabel('T_c (s)')
    axis([0 11 0 6])
    hold on
    yyaxis right
    plot(fork_prob(l,:),'-s','linewidth',2.0)
    title(titles{l})
    axis([0 11 0 1])
    xlabel('Block size')
    ylabel('Fork probability')
    legend({'IEEE 802.11ax','5G NR X2/Xn'})
    set(gca,'FontSize',18,'FontName','Times')
end

%% Boxplot of the transaction conf. delay for different lambda and block_size
fig = figure;
for l = 1:length(lambda_master)
subplot(1,length(lambda_master),l)
data = [];
grp = [];
for s = 1 : length(block_size_master)
    mean_total_delay(s) = mean(total_d_service{l,s}(total_d_service{l,s}>0));
    mean_total_delay2(s) = mean(total_d_spectrum{l,s}(total_d_spectrum{l,s}>0));
end
end
% Delay Service BC
subplot(1,2,1)
boxplot(mean_total_delay')
hold on
axis([0 4 0 5.6])
xlabel('Block size (#trans.)')
ylabel('Total delay (s)')
xticks(1:length(block_size_master))
xticklabels(1:length(block_size_master))
grid on
grid minor
set(gca,'FontSize',16,'FontName','Times')
% Delay Spectrum BC
subplot(1,2,2)
boxplot(mean_total_delay2')
hold on
axis([0 4 0 5.6])
xlabel('Block size (#trans.)')
ylabel('Total delay (s)')
xticks(1:length(block_size_master))
xticklabels(1:length(block_size_master))
grid on
grid minor
set(gca,'FontSize',16,'FontName','Times')
