# Blockchain-enabled RAN Sharing Simulator #

## Table of Contents
- [Authors](#authors)
- [Repository description](#repositorydescription)
- [Usage](#usage)
- [Projects](#projects)
- [Contribute](#contribute)

## Authors
* [Lorenza Giupponi](http://www.cttc.es/people/lgiupponi/)
* [Francesc Wilhelmi](https://fwilhelmi.github.io/)

## Repository description

This repository contains the "Blockchain-enabled RAN sharing simulator", which has been developed to evaluate the performance of BC when used in networks. The simulator includes a predefined cellular-based deployment with a fixed number of cells and randomly deployed User Equipment (UE) devices, which number is defined by an input parameter. To obtain data services from operators UEs can use a Blockchain by submitting transactions where service parameters (e.g., throguhput required, duration) are detailed. In parallel, operators can exchange network resources dynamically through a different Blockchain. The way transactions and blocks are propagated is ruled by either IEEE 802.11ax-based links or NR X2/Xn interfaces, thus incurring into additional delay and overhead.

The code is structured as follows:

1. Classes: contains the files that define classes such as Blockchain, Block or Transaction.
2. Methods: contains all the methods used by the simulator, which are related to the deployment (e.g., compute signal propagation effects), the blockchain (e.g., add/remove transactions), the auction (e.g., determine the operator to deliver service to a specific user), etc.
3. Simulations: contains the files for running the simulations, including configuration parameters and output gathering.
4. Output: empty folder where the simulator saves the output .mat files.
5. tmp: empty folder used to store temporary resources, such as the configuration of a simulation.
6. RunSimulation.m: simulation engine that gets parameters from the main files and executes the simulation.
7. initialize_variables.m: file used to initialize variables used by the main simulation engine (RunSimulation.m)
8. constants.m: file containing constants that are used during the simulation.

## Usage

An example is provided for preparing the simulation and executing it. It can be found at "Simulations" folder:

1. conf_simulation.m: defines all the parameters to characterize the simulation, including simulation time, path-loss model, number of UEs, number of operators, Blockchain parameters (e.g., block size, timeout, mining rate), etc.
2. main_example.m: gathers all the simulation parameters and executes one or multiple simulations, depending on the parameters defined in the conf_simulation.m file.

**Disclaimer:** The "Signal Processing Toolbox" is required to execute some of the functions included in the simulator.


## Projects

This simulator has been used to generate results for the following academic publications:

[1] Wilhelmi, F., & Giupponi, L. (2021). On the Performance of Blockchain-enabled RAN-as-a-service in Beyond 5G Networks. arXiv preprint arXiv:2105.14221.

[2] Giupponi, L., & Wilhelmi, F. (2021). Blockchain-enabled Network Sharing for O-RAN

## Contribute

If you want to contribute, please contact to [lorenza.giupponi@cttc.es](lorenza.giupponi@cttc.es) and [fwilhelmi@cttc.cat](fwilhelmi@cttc.cat)